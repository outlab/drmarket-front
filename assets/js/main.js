$(document).ready(function(){

    $("#keyone").click(function() {
        $(".middlerow").toggle("fast");
        $(".bottomrow").toggle("fast");
        $(this).toggleClass("active");
    });

    //

    $(".openmenu").click(function() {
      $(".menu").toggle();
    });

    $(".searchicon").click(function() {
      $(".searchbar").toggle();
    });

    $(".opensub").click(function() {
      $(".submenu").toggle();
    });

    $(".opensubprod").click(function() {
      $(".submenuprod").toggle();
    });

    $(".openpopup").click(function() {
      $(".popup").toggle();
      $(".shadowpop").toggle();
    });

    $(".payment .method .card").click(function() {
      $(".cardform").toggle();
      $(".paperform").hide();
    });

    $(".payment .method .paper").click(function() {
      $(".paperform").toggle();
      $(".cardform").hide();
    });

    $(".orderitem .orderdetails .openorder").click(function() {
      $(".plusorder").toggle();
    });
    
    //

    $(".sellerresume .opencartprod").click(function() {

      var cardlistid1 = $(this).attr("id");

      $(".freteapartir").toggle();

      $("." + cardlistid1 + " .productbox").toggle();
      $(this).toggleClass("active");
    });

    // ANCHOR

    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top - 150
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });

})

/************ JS *******************/

// FAQ

document.addEventListener("DOMContentLoaded", function(event) { 

  var acc = document.getElementsByClassName("faqquestion");
  var panel = document.getElementsByClassName('faqanswer');
  
  for (var i = 0; i < acc.length; i++) {
      acc[i].onclick = function() {
          var setClasses = !this.classList.contains('active');
          setClass(acc, 'active', 'remove');
          setClass(panel, 'show', 'remove');
  
          if (setClasses) {
              this.classList.toggle("active");
              this.nextElementSibling.classList.toggle("show");
          }
      }
  }
  
  function setClass(els, className, fnName) {
      for (var i = 0; i < els.length; i++) {
          els[i].classList[fnName](className);
      }
  }
  
});